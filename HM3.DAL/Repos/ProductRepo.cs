﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM3.DAL.IDao;
using HM3.Models;

namespace HM3.DAL.Repos
{
    public  class ProductRepo:AbstractDao<SalesEntities,Product>
    {
        public List<Product> getProductsBySellerId(object SId)
        {
            var all = this.getAll();
            return all.Where(x => x.sellerId.Equals(SId)).ToList();
        }
    }
}
