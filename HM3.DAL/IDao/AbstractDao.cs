﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Validation;
using System.Linq;
using Interfaces;
using EntityState = System.Data.Entity.EntityState;

namespace HM3.DAL.IDao
{
    public abstract class AbstractDao<T, C>:IRepository<C>,ISubject where C : class where T : DbContext, new()
    {

        private T _entities;

        public T Context
        {

            get { return _entities; }
            set { _entities = value; }
        }
        private static List<IObserver> observers=new List<IObserver>();
        public  void Subscribe(IObserver observer)
        {
            observers.Add(observer);
        }

        public void Unsubscribe(IObserver observer)
        {
            observers.Remove(observer);
        }

        public void Notify()
        {
            var t = observers;
            t.ForEach(x => x.Update());
        }
        public void Add(C entity)
        {
            using ( _entities = new T())
            {

                _entities.Set<C>().Add(entity);
                _entities.Entry(entity).State = EntityState.Added;
                var t = _entities.Set<C>().ToList();
                this.Save();
            }
        }
       public List<C> getAll()
        {
            using (_entities = new T())
            {
                return _entities.Set<C>().ToList();
            }

        }
        public void UpdateById(C entity,int id)

        {
            using (_entities = new T())
            {
                var toBeUpdated = _entities.Set<C>().Find(id);
                _entities.Entry(toBeUpdated).CurrentValues.SetValues(entity);
                this.Save();
            }
        }

   
        public void Save()
        {
           
                try
            {
                
                    _entities.SaveChanges();
                    this.Notify();
                
            }
            catch (DbEntityValidationException e)
            {
                var t = e.ToString();
            }
            catch (Exception e)
            {
                var t = e.ToString();
            }
        }

        public void Insert(C entity)
        {
            using (_entities = new T())
            {
                _entities.Set<C>().Add(entity);
                this.Save();
            }
        }

        public void UpdateById(C entity, object id)
        {
            using (_entities = new T())
            {
                var t = _entities.Set<C>().Find(id);
                _entities.Entry(t).CurrentValues.SetValues(entity);
                this.Save();
            }
        }
        public  void DeleteById(object id)
       {

           try
           {
               using (_entities = new T())
               {
                   var result = this._entities.Set<C>().Find(id);
                   this._entities.Set<C>().Attach(result);

                   this._entities.Set<C>().Remove(result);
                   this._entities.SaveChanges();
                   //  _entities.Set<C>().Attach()
                   //  _entities.Set<C>().Remove(result);

                   this.Save();
               }
           }
           catch (EntityException e)
           {
               var t = e.ToString();
           }
           catch (Exception e)
           {
               var t = e.ToString();
           }
        }
    }
}
