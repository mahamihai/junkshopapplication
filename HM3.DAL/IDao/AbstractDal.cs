﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;

namespace HM3.DAL.IDao
{
    public abstract class AbstractDal<T> where T : class
    {
        public ISession getConnection()
        {
            var cfg = new Configuration();



            cfg.DataBaseIntegration(x =>
            {

                x.ConnectionString = " Data Source = localhost; Initial Catalog =Sales; Integrated Security = True";




                x.Driver<SqlClientDriver>();
                x.Dialect<MsSql2008Dialect>();

            });

            cfg.AddAssembly(Assembly.GetExecutingAssembly());
            var t = Assembly.GetExecutingAssembly();
            var sefact = cfg.BuildSessionFactory();
            return sefact.OpenSession();

        }

        public IEnumerable<T> getAll()
        {

            using (var session = getConnection())
            {

                using (var tx = session.BeginTransaction())
                {
                    return session.Query<T>().ToList();
                }
            }
        }
        public string insert(T obj)
        {
            try
            {
                using (var session = getConnection())
                {

                    using (var tx = session.BeginTransaction())
                    {
                        session.Save(obj);
                        tx.Commit();

                    }
                }

                return "Succces";
            }
            catch ( Exception e)

            {
                var t = e.ToString();
                return "Failed";
            }
        }
        public string updateUser(T user)
        {

            try
            {
                using (var session = getConnection())
            {

                using (var tx = session.BeginTransaction())
                {
                    session.SaveOrUpdate(user);
                    tx.Commit();
                }
            }
                return "Succces";
            }
            catch (Exception e)

            {
                var t = e.ToString();
                return "Failed";
            }
        }
        public string delete(object id)
        {
            try
            {
                using (var session = getConnection())
                {

                    using (var tx = session.BeginTransaction())
                    {
                        var t = session.Get<T>(id);
                        session.Delete(t);
                        tx.Commit();


                    }
                }

                return "Succces";
            }
            catch (Exception e)

            {
                var t = e.ToString();
                return "Failed";
            }
        }
    }
}
