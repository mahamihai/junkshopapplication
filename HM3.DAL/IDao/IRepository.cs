﻿using System.Collections.Generic;

namespace HM3.DAL.IDao
{
    public interface IRepository<T> where T : class
    {

       
        void Add(T entity);
        List<T> getAll();
        void UpdateById(T entity,int id);
        void Insert(T entity);
        void UpdateById(T entity,object id);
        void DeleteById(object id);
    }
}
