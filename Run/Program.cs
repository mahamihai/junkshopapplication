﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM3.CONTROLLER;
using HM3.DAL.Server;
using HM3.VIEW;

namespace Run
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AsynchronousSocketListener server=new AsynchronousSocketListener();
            new Thread(() =>
            {
                server.StartListening();

            }).Start();
           
            Login loginController = new Login();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            


             Application.Run(new LoginForm(loginController));
        }
    }
}
