﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using HM3.DAL.Dao;
using HM3.DAL.Repos;
using HM3.Models;
using HM3.VIEW.IController;
using Interfaces;
using Newtonsoft.Json;

namespace HM3.CONTROLLER
{
    public class Buyer : IBroker
    {
        private ProductRepo _pRepo;
        private UserRepo _uRepo;
        private OrderRepo _oRepo;
        private User _user;
        private List<Product> shoppingBasket=new List<Product>();
        private SoldRepo _sRepo;

        public double GetMoney()
        {
            var user = this._uRepo.getAll().Where(x => x.id.Equals(this._user.id)).First();
            this._user = user;
            
            return this._user.money;
        }
        AsynchronousClient _client= new AsynchronousClient();
        public Buyer(User user)
        {
            this._user = user;
            this._pRepo = new ProductRepo();
            this._uRepo = new UserRepo();
            this._oRepo = new OrderRepo();
            this._sRepo=new SoldRepo();
            _client.StartClient();
        }
        
        public void RegisterView(IObserver f)
        {
            this._pRepo.Subscribe(f);

        }

        public List<Product> getAllProducts()
        {
            this._client.Receive(this._client.client);
            this._client.Send("allProducts<EOF>");
            System.Threading.Thread.Sleep(400);
            var t = this._client.Response;
            var products = JsonConvert.DeserializeObject<List<Product>>(t);
            return products;
        }

        public void updateItems()
        {
            var products = this._pRepo.getAll();

            for (int i=this.shoppingBasket.Count-1;i>-1;i--)
            {
               
                    var prod = products.Where(x => x.id.Equals(shoppingBasket[i].id)).First();
                if (prod != null)
                {
                    shoppingBasket[i].price = prod.price;
                }
                else
                {
                    this.shoppingBasket.RemoveAt(i);
                }

              
            }
        }

        void updateMoney(double amount)
        {
            this._user.money -= amount;
            this._uRepo.UpdateById(this._user,this._user.id);
        }
        private bool checkAvailableProducts()//vezi daca produsele mai sunt disponibile
        {
            var prods = this._pRepo.getAll();
            var products = copyList<Product>(prods);
            bool ok = true;
            foreach (var tmp in shoppingBasket)
            {
                var t = products.Where(x => x.id.Equals(tmp.id)).First();
                if (t.stock < tmp.stock)
                {
                    ok = false;
                }
                else
                {
                    t.stock -= tmp.stock;
                }
            }

            return ok;
        }

        private double getCheckoutValue()//vezi daca ai destui bani
        {
            double sum = 0;
            foreach (var tmp in shoppingBasket)
            {
                sum += tmp.stock * tmp.price;
            }

            return sum ;
        }

        public void updateMoney(int sellerId, double value)
        {
            var user = this._uRepo.getAll().Where(x => x.id.Equals(sellerId)).FirstOrDefault();
            if (user != null)
            {
                user.money += value;
            }
            this._uRepo.UpdateById(user,user.id);
        }
        public string checkOut()//cumpara ce ai in cos
        {
            var products1 = this._pRepo.getAll();

            double cost = getCheckoutValue();
            if (cost<=this._user.money && checkAvailableProducts())
            {
                Order newOrder = new Order
                {
                    total = cost,
                    clientId = this._user.id
                };
                this._oRepo.Add(newOrder);

                
                foreach (var tmp in shoppingBasket)
                {
                    var products = copyList<Product>(this._pRepo.getAll());
                    var prod = products.Where(x => x.id.Equals(tmp.id)).First();
                    prod.stock -= tmp.stock;
                    updateMoney(tmp.sellerId, tmp.stock * tmp.price);
                    this._pRepo.UpdateById(prod,prod.id);
                    Sold newSold = new Sold
                    {
                        orderId = newOrder.id,
                        productId = tmp.id,
                        price = tmp.price,
                        stock = tmp.stock

                    };
                   
                    this._sRepo.Add(newSold);
                    

                }
                updateMoney(cost);
                return "Succes";
            }
            else
            {
                return "Fail";
            }
        }
     
        public static T Clone<T>(T source)
        {
            var serialized = JsonConvert.SerializeObject(source);
            return JsonConvert.DeserializeObject<T>(serialized);
        }

        private List<T> copyList<T>(List<T> src)
        {
            List<T> newL=new List<T>();
            foreach (var tmp in src)
            {
                Type m = typeof(T);
                var newO = Activator.CreateInstance(m);
                foreach (var t in m.GetProperties())
                {
                    var value = t.GetValue(tmp);
                    t.SetValue(newO,value);
                }
                newL.Add((T)newO);
            }

            return newL;
        }
        public void addItem(int productId,int quantity)
        {
            try
            {
                var products = copyList<Product>(this._pRepo.getAll());
                 var p = products.Where(x => x.id.Equals(productId)).First();
                if (p != null)
                {
                    if (p.stock >= quantity)
                    {
                      
                        p.stock = quantity;
                        this.shoppingBasket.Add(p);
                    }
                }
            }
            catch(Exception e)
            {
                var t = e.ToString();
            }
            var products1 = this._pRepo.getAll();

        }

        public List<Product> getBasket()
        {
            return this.shoppingBasket;
        }



    }
}
