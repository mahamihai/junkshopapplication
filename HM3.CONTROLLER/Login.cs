﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HM3.DAL.Dao;
using HM3.Models;
using HM3.VIEW;
using HM3.VIEW.IController;

namespace HM3.CONTROLLER
{
   public class Login:ILogin
   {
       private UserRepo _userRepo;
        public Login()
        {
            this._userRepo=new UserRepo();
           

            
            
        }
      public   string checkUser(string username,string password)
      {
          try
          {
             
              var user = this._userRepo.getAll().Where(x => x.username.Trim().Equals(username) &&
                                                            x.password.Trim().Equals(password)).ToList().First();
              if (user != null)
              {
                  if (user.status.Equals("seller"))
                  {
                        ISeller sController=new Seller(user);
                        SellerForm sForm=new SellerForm(sController);
                        sController.RegisterView(sForm);
                      sForm.Show();
                  }
                  else
                  {
                           IBroker bController=new Buyer(user);
                           BrokerForm bForm=new BrokerForm(bController);
                         bController.RegisterView(bForm);
                         bForm.Show();
                  }

                  return "Success";
              }
              else
              {
                  return "Bad credentials";
              }
          }
          catch(Exception e)
          {
              var t = e.ToString();
              Console.WriteLine(t);
              return "Failure";
          }
      }
    }
}
