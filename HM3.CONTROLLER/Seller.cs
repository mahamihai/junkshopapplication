﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM3.DAL.Dao;
using HM3.DAL.Repos;
using HM3.Models;
using HM3.VIEW;
using HM3.VIEW.IController;
using Interfaces;

namespace HM3.CONTROLLER
{
   public class Seller:ISeller
   {
       private ProductRepo _pRepo;
       private UserRepo _uRepo;
       private User _user;
        public Seller(User user)
        {
            this._pRepo=new ProductRepo();
           
            this._user = user;
            this._uRepo=new UserRepo();
        }
        public List<Product> getProducts()
        {
            return this._pRepo.getProductsBySellerId(this._user.id);
        }

       public void RegisterView(IObserver f)
       {
           this._pRepo.Subscribe(f);
           
       }

      public double GetMoney()
      {
          var user = this._uRepo.getAll().Where(x => x.id.Equals(this._user.id)).First();
          this._user = user;
          return this._user.money;
      }
        public string AddProduct(Product prod)

       {
           try
           {
               prod.sellerId = this._user.id;

                this._pRepo.Add(prod);
               return "Success";
           }
           catch
           {
               return "Failure";
           }
       }

    



    }
}
