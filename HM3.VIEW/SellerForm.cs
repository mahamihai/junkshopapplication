﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM3.Models;
using HM3.VIEW.IController;
using Interfaces;

namespace HM3.VIEW
{
    public partial class SellerForm : Form, IObserver
    {
        private SellerForm()
        {
            InitializeComponent();

        }

        delegate void Del();

        private Del refreshData;

        private void UpdateMoney()
        {
            double money = this._sController.GetMoney();
            this.textBox1.Text = money.ToString();
        }
        public void Update()
        {
            UpdateMoney();
            this.refreshData();
        }

        private ISeller _sController;
        public SellerForm(ISeller seller)
        {
            InitializeComponent();
            this._sController = seller;
          

        }

        private void GetProducts()
        {
            var products = this._sController.getProducts();
            this.dataGridView1.DataSource = products;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.refreshData = GetProducts;
           this.Update();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Product prod = new Product
            {
                title = this.textBox2.Text,
                stock = Int32.Parse(this.textBox3.Text),
                description = this.textBox4.Text,
                price = Double.Parse(this.textBox5.Text),
                label = this.textBox6.Text

            };
            this._sController.AddProduct(prod);
        }

        private void SellerForm_Load(object sender, EventArgs e)
        {

        }
    }
}
