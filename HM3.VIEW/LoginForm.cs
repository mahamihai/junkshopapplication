﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM3.VIEW.IController;

namespace HM3.VIEW
{
    public partial class LoginForm : Form
    {
        
        private ILogin _loginController;
        public LoginForm(ILogin log)
        {
            InitializeComponent();
            this._loginController = log;
        }
        
        private LoginForm()
        {
            InitializeComponent();

        }

        public void button1_Click(object sender, EventArgs e)
        {
            string username = this.textBox1.Text;
            string password = this.textBox2.Text;
            MessageBox.Show(this._loginController.checkUser(username, password));

        }
    }
}
