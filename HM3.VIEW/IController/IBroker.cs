﻿
using System.Collections.Generic;
using System.Net;
using HM3.Models;
using Interfaces;

namespace HM3.VIEW.IController
{
    
    public interface IBroker
   {
       double GetMoney();
       void RegisterView(IObserver f);
       List<Product> getAllProducts();
        void addItem(int productId, int quantity);
       List<Product> getBasket();
       void updateItems();
       string checkOut();



   }
}
