﻿using System.Collections.Generic;
using System.Net;
using HM3.Models;
using Interfaces;

namespace HM3.VIEW.IController
{
    public interface ISeller
    {
        List<Product> getProducts();
        void RegisterView(IObserver f);
        double GetMoney();
        string AddProduct(Product prod);
    }
}
