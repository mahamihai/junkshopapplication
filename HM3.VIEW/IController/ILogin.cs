﻿namespace HM3.VIEW.IController
{
   public interface ILogin
   {
       string checkUser(string username, string password);
   }
}
