﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HM3.VIEW.IController;
using Interfaces;

namespace HM3.VIEW
{
    public partial class BrokerForm : Form, IObserver
    {
        private BrokerForm()
        {
            InitializeComponent();
        }
        private IBroker _bController;
        public BrokerForm(IBroker control)
        {
            InitializeComponent();
            this._bController = control;
        }
        delegate void Del();

        private Del refreshData;

        private void UpdateMoney()
        {
            double money = this._bController.GetMoney();
            this.textBox2.Text = money.ToString();

        }
       
        public void Update()
        {
            UpdateMoney();
            this.refreshData();
        }

        public void DisplayAllProducts()
        {
            var all = this._bController.getAllProducts();
            this.dataGridView1.DataSource = all;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.refreshData = delegate
            {
                DisplayAllProducts();
                this.UpdateMoney();
            };
            this.Update();
        }

        private int selectedId;
        private void dataGridView1_CellContentClick2(object sender, DataGridViewCellEventArgs e)
        {
            int id;
           
            double quantity;
            try
            {
                int row = e.RowIndex;
                string value = this.dataGridView1.Rows[row].Cells["id"].Value.ToString();
                id = int.Parse(value);
            }
            catch (Exception ex)
            {
                string t = ex.ToString();
                return;
            }

            this.selectedId = id;

        }
    
        private void button2_Click(object sender, EventArgs e)
        {
            this.refreshData = delegate
            {
                this.dataGridView1.DataSource = this._bController.getBasket();
                this._bController.updateItems();
                this.UpdateMoney();
            };
            this.Update();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int quantity = Int32.Parse(this.textBox1.Text);
            this._bController.addItem(this.selectedId,quantity);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this._bController.checkOut());
            this.Update();
        }
    }
}
